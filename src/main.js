import Vue from 'vue'
import App from './App.vue'
import VueBreadcrumbs from 'vue-2-breadcrumbs'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import firebase from './plugins/firebase'
import './registerServiceWorker'

Vue.config.productionTip = false
Vue.use(firebase)
Vue.use(VueBreadcrumbs)

new Vue({
  router,
  VueBreadcrumbs,
  firebase,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
