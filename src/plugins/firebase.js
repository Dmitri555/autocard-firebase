import firebase from 'firebase'
export default {
  install (Vue, options) {
    const app = firebase.initializeApp(
      {
        apiKey: 'AIzaSyB_pScP1WL6pmo6nTRqWJpLrplCEePitJE',
        authDomain: 'autocard-b22c9.firebaseapp.com',
        databaseURL: 'https://autocard-b22c9.firebaseio.com',
        projectId: 'autocard-b22c9',
        storageBucket: 'autocard-b22c9.appspot.com',
        messagingSenderId: '557619814871',
        appId: '1:557619814871:web:8309a802184c03db372b92'
      })
    Vue.prototype.$firebase = app
    Vue.prototype.$auth = firebase.auth
    Vue.prototype.$db = app.firestore()
    Vue.prototype.$storage = app.storage()
  }
}
