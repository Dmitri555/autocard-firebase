import Vue from 'vue'
import VueRouter from 'vue-router'
import ProfileForm from '../components/DriverProfileForm.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/driver/profile',
    name: 'profile',
    component: ProfileForm
  }
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
export default router
